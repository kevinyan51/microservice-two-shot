import React from 'react';
import {useEffect, useState} from "react"


function HatsList() {
  const [hats, setHats] = useState([]);

  const fetchData = async () => {
    const url = 'http://localhost:8090/api/hats/';
    const response = await fetch(url);
    if(response.ok) {
      const data = await response.json();
      setHats(data.hats);
      console.log(setHats);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);
const deleteHat = async (id) => {
    const deleteUrl = `http://localhost:8090/api/hats/${id}/`;
    const response = await fetch(deleteUrl, {method: "DELETE"});
        if (response.ok) {
            fetchData();
        } else {
            console.log(`Failed to fetch ID ${id}`)
        }
    console.log(deleteHat)     
  }
    
    return (
        <table className="table table-striped">
        <thead>
          <tr>
            <th>Style Name</th>
            <th>Fabric</th>
            <th>Color</th>
            <th>Picture_URL</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
          {hats.map(hat => {
            return (
              <tr key={ hat.id }>
                <td>{ hat.style_name }</td>
                <td>{ hat.fabric }</td>
                <td>{ hat.color }</td>
                <td className="align-middle"><img src={hats.picture_url} alt="" height="100" width="100"></img></td>
                <td>{ hat.location }</td>
                <td>
                    <button type="button" className="btn btn-danger" onClick={ () => deleteHat(hat.id)}>Delete</button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
}
export default HatsList;




























