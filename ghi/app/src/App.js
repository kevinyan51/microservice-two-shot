import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import CreateHatForm from './CreateHatForm'
import HatsList from './HatsList';
import CreateShoeForm from './CreateShoeForm'
import ShoesList from './ShoesList'
import {useState, useEffect} from 'react';


function App(props) {
  const [shoes, setShoes] = useState([])
  const [hats, setHats] = useState([])
  const [bins, setBins] = useState([])
  const [locations, setLocations] = useState([])

  const getHats = async () => {
    const hatsUrl = 'http://localhost:8090/api/hats/'
    const hatsResponse = await fetch(hatsUrl)

    if (hatsResponse.ok) {
      const data = await hatsResponse.json();
      const hats = data.hats;
      setHats(hats);
    }
  }
  const getShoes = async () => {
    const shoeUrl = 'http://localhost:8080/api/shoes/'
    const shoeResponse = await fetch(shoeUrl)

    if (shoeResponse.ok) {
      const data = await shoeResponse.json();
      const shoes = data.shoes;
      setShoes(shoes);
    }
  }
  const getBins = async () => {
    const binUrl = 'http://localhost:8100/api/bins/'
    const binResponse = await fetch(binUrl)

    if (binResponse.ok) {
      const data = await binResponse.json();
      const bins = data.bins;
      setBins(bins);
    }
  }
  const getLocations = async () => {
    const locationUrl = 'http://localhost:8100/api/locations/'
    const locationResponse = await fetch(locationUrl)

    if (locationResponse.ok) {
      const data = await locationResponse.json();
      const locations = data.locations;
      setLocations(locations);
    }
  }

  useEffect( () => {getShoes(); getBins(); getLocations(); getHats()}, [])
  
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage/>} />
          <Route path= "/hats" >
            <Route path="" element={<HatsList hats={hats} getHats={getHats} />} />
            <Route path="new" element={<CreateHatForm locations={locations} getHats={getLocations} />} />
          </Route>
          <Route path='/shoes'>
            <Route path='' element={<ShoesList shoes={shoes} getShoes={getShoes} />} />
            <Route path='new' element={<CreateShoeForm bins={bins} getShoes={getShoes}  />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;


