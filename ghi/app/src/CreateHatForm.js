import React, { useEffect, useState } from 'react';


function CreateHatForm() {
    const [locations, setLocations] = useState([]);
    const [location, setLocation] = useState('');
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    };
    const [color, setColor] = useState('');
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    };
    const [picture_url, setPictureUrl] = useState('');
    const handlePictureUrlChange = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    };
    const [fabric, setFabric] = useState('');
    const handleFabricChange = (event) => {
        const value = event.target.value;
        setFabric(value);
    };
    const [style_name, setStyleName] = useState('');
    const handleNameChange = (event) => {
        const value = event.target.value;
        setStyleName(value);
    };
    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.fabric = fabric;
        data.style_name = style_name;
        data.color = color;
        data.picture_url = picture_url;
        data.location = location;
        

        const hatsUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-type': 'application/json',
            },
        };
        
        const response = await fetch(hatsUrl, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            

            setFabric('');
            setStyleName('');
            setColor('');
            setLocations([]);
            setPictureUrl('');
        }
    }
    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
            console.log(locations);
            console.log(fetchData);
            
        }
    }
    
    useEffect(() => {
        fetchData();
      }, []);

    
    return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new Hat</h1>
              <form onSubmit={handleSubmit}  id="create-location-form">
                <div className="form-floating mb-3">
                  <input onChange={handleFabricChange} value={fabric} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                  <label htmlFor="fabric">Fabric</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleNameChange} value={style_name}   placeholder="Style Name" required type="text" name="style_name" id="style_name" className="form-control" />
                  <label htmlFor="style_name">Style Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleColorChange} value={color}  placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                  <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handlePictureUrlChange} value= {picture_url}  placeholder="Picture Url" required type="text" name="picture_url" id="picture_url" className="form-control" />
                  <label htmlFor="picture_url">Picture Url</label>
                </div>
                <div className="mb-3">
                  <select onChange={handleLocationChange} value={location}  required name="location" id="location" className="form-select">
                    <option value="">Choose a location</option>
                    {locations.map(location => {
                        return (
                            <option value={location.href} key={location.href} >
                                {location.closet_name}
                            </option>
                        );
                    })}
                    
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      );
}

export default CreateHatForm