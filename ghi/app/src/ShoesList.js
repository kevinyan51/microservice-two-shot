import React from 'react';
import {useEffect, useState} from "react"


function ShoesList() {
  const [shoes, setShoes] = useState([]);

  const fetchData = async () => {
    const url = 'http://localhost:8080/api/shoes/';
    const response = await fetch(url);
    if(response.ok) {
      const data = await response.json();
      setShoes(data.shoes);
      console.log(setShoes);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

const deleteShoe = async (id) => {
    const deleteUrl = `http://localhost:8080/api/shoes/${id}/`;
    const response = await fetch(deleteUrl, {method: "DELETE"});
        if (response.ok) {
            fetchData();
        } else {
            console.log(`Failed to fetch ID ${id}`)
        }
        
}
    
    return (
        <table className="table table-striped">
        <thead>
          <tr>
            <th>Manufacturer</th>
            <th>Name</th>
            <th>Color</th>
            <th>Picture_URL</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
          {shoes.map(shoes => {
            return (
              <tr key={shoes.id}>
                <td>{ shoes.manufacturer }</td>
                <td>{ shoes.name }</td>
                <td>{ shoes.color }</td>
                <td className="align-middle"><img src={shoes.picture_url} alt="" height="100" width="100"></img></td>
                <td>
                    <button type="button" className="btn btn-danger" onClick={ () => deleteShoe(shoes.id)}>Delete</button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );

}


export default ShoesList;


// console.log(getShoes);
    // const deleteShoe = async (id) => {
    // fetch(`http://localhost:8080/api/shoes/${id}`, {
    //     method: "delete"
    // })
    // .then(() => {
    //     return getShoes()
    //     }).catch(console.log)
    // }
    // if(shoes === undefined) {
    //     return null;
    // }
