from django.shortcuts import render
from .models import Shoes, BinVO
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from django.http import JsonResponse
import json

class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "bin_number",
        "import_href",
        "bin_size",
        "closet_name",
    ]

class ShoesListEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "name",
        "color",
        "picture_url",
        "manufacturer",
        "bin",
        'id',
    ]
    encoders={
        "bin": BinVOEncoder(),
    }

class ShoeDetailEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "name",
        "color",
        "picture_url",
        "bin",
    ]
    encoders={
        "bin": BinVOEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        if bin_vo_id is not None:
            shoes = Shoes.objects.filter(bin=bin_vo_id)
        else:
            shoes = Shoes.objects.all()
        return JsonResponse(
            {'shoes': shoes},
            encoder= ShoesListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )

        shoe = Shoes.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET"])
def api_show_shoes(request, pk):
    if request.method == "GET":
        shoe = Shoes.objects.get(id=pk)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
    if request.method == "DELETE":
        count, _ = Shoes.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
